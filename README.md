# UE3DW20 : Debug Front-end  - Semaine 3 - Groupe A

*Vincent Pichot - Idriss Driouch*

https://framagit.org/Pich/groupea-s3-3dw20-2018

## Problème : seule une page noire s'affiche

### Solution :

#### Etape 1 : Validateur W3C

Le passage au [validateur W3C](https://validator.w3.org/) nous permet de localiser rapidement les erreurs de syntaxe HTML et de les corriger, le but étant d'obtenir un fichier de travail valide.

*Erreurs remontées : 5*

*Erreurs corrigées : 5*

#### Etape 2 : Devtools

Ici il s'agit de procéder de facon systématique et de corriger les erreurs dans l'ordre d'apparition.

*Erreurs remontées : 3*

*Erreurs corrigées : 3*

````
Failed to load resource: the server responded with a status of 404 (Not Found)
script.js:22 Uncaught SyntaxError: Unexpected token )
preloader.csss:1 Failed to load resource: the server responded with a status of 404 (Not Found)
````

La correction d'une erreur de typo nous a permis de charger la feuille de style `preloader.css`.

Quant au fichier `script.js`, une seule remontée d'erreur était présente, cependant il est apparu évident à la lecture du fichier qu'il contenait plusieurs erreurs. Grâce au plugin `eslint` de l'IDE, les erreurs les plus évidentes ont pu être corrigées. Il a notamment fallu éditer le code car l'indentation était mauvaise.

Par ailleurs, l'ordre de chargement des scripts Javascript dans le fichier `index.php` a dû être modifié de façon à avoir l'appel de JQuery avant celui du fichier `script.js` puisque celui-ci en dépend.

## Problème résolu : la page météo s'affiche

![Meteo-Limoges](assets\debug\images\Meteo-Limoges.png)



## Problème : des erreurs en console persistent

````
Uncaught TypeError: $puces.removeClass(...).first.addClass is not a function
    at HTMLDivElement.<anonymous> (script.js:11)
    at HTMLDivElement.d.complete (jquery.min.js:4)
    at j (jquery.min.js:2)
    at Object.fireWith [as resolveWith] (jquery.min.js:2)
    at i (jquery.min.js:4)
    at n.fx.tick (jquery.min.js:4)
````

Pour corriger cette erreur il a fallu remplacer :

````js
$puces.removeClass('active').first.addClass('active');
````

par :

````js
$puces.removeClass('active').first().addClass('active');
````

`first()` étant une [méthode de l'API jQuery](https://api.jquery.com/first/#first).

## Problème résolu : il n'y a plus d'erreur en console

Cependant d'autres problèmes persistent. 

Ce qui a été corrigé :

- Changement de l'extension de fichier `index.php` par `index.html` étant donné que le fichier n'utilise pas *php*
- Correction d'une erreur dans le chemin d'une image (`gps_on.jpg`) dans le fichier `style.css` Cette erreur a pu être décelée en utilisant l'outil *Network* des *DevTools*. En effet, le chemin vers ce fichier, qui était inexact, renvoyait une erreur 404.

## Organisation du groupe

Un [dépôt Framagit](https://framagit.org/Pich/groupea-s3-3dw20-2018) et un serveur Discord ont été créés pour organiser et cadrer le travail collaboratif autour de ce projet. Sur les 3 étudiants qui étaient avec moi dans le groupe, seul Idriss Driouch a répondu présent. Cependant Idriss Driouch n'a pas contribué au projet et n'a pas manifesté une réelle volonté de participer alors que je me suis mis à sa disposition pour l'aider à mettre en place Git. C'est pourquoi j'ai réalisé le travail seul.

